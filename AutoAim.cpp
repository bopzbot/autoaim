#include <iostream>     // std::cout
#include <windows.h>    // Sleep, GetPixel
#include <winuser.h>    // SetCursorPos, mouse_event, GetAsyncKeyState, GetDC, HDC
#include <thread>       // Threads

typedef struct {
    int x;
    int y;
} Point;

void welcomeMessage() {
    std::cout << "\n \n \n";
    std::cout << "         ____ ____ ____ ____ ____ ____ ____ \n";
    std::cout << "        ||B |||o |||p |||z |||. |||i |||o || \n";
    std::cout << "        ||__|||__|||__|||__|||__|||__|||__|| \n";
    std::cout << "        |/__\\|/__\\|/__\\|/__\\|/__\\|/__\\|/__\\| \n";
    std::cout << "        ||A |||u |||t |||o |||A |||i |||m || \n";
    std::cout << "        ||__|||__|||__|||__|||__|||__|||__|| \n";
    std::cout << "        |/__\\|/__\\|/__\\|/__\\|/__\\|/__\\|/__\\| \n";
    std::cout << "\n \n \n";
    std::cout << "        Made by Midi & Kilian we Hope you enjoy = }";
}


void click(Point* enemy) {
    // dial in the timings
    int x;
    int y;
    while (true) {
        x = enemy->x;
        y = enemy->y;
        if (!(x <= 0 || y <= 0)) {
            SetCursorPos(x, y);
            mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, 0);
            mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
            Sleep(2);
            mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
            Sleep(1);
            mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, 0);
            Sleep(1);
            //std::cout << "DEBUG: found person";
        }
    }
}

void radialSearch(const int width, const int height, DWORD* pPixels, Point* enemy) {

    int centerX = (int)width / 2;
    int centerY = (int)height / 2;

    if (enemy->x > 0 && enemy->y > 0) {
        int centerX = enemy->x;
        int centerY = enemy->y;
    }

    int x = centerX;
    int y = centerY;

    const int stepSize = 5;

    int dx = 1;
    int dy = 0;

    int steps = stepSize * 1;

    while (x < width - stepSize) {
        if (dx == 1) {
            if (x > (steps + centerX) || x >= width - stepSize) {
                dx = 0;
                dy = -1;
            }
        }
        else if (dx == -1) {
            if (x < (steps - centerX) || x <= stepSize) {
                dx = 0;
                dy = 1;
            }
        }
        else if (dy == 1) {
            if (y > (steps + centerY) || y >= height - stepSize) {
                dy = 0;
                dx = 1;
                steps += stepSize;
            }
        }
        else {
            if (y < (steps - centerY) || y <= stepSize) {
                dy = 0;
                dx = -1;
            }
        }

        x += dx;
        y += dy;

        switch ((pPixels[x + y * width])) {
            // skin colors
        case(0xffffead3):   //0x00d3eaff
        case(0xfffadab8):   //0x00b8dafa
        case(0xffdeb991):   //0x0091b9de
        case(0xffbd8e68):   //0x00688ebd
        //case(0xff9a643c):   //0x003c649a
        case(0xff584539):   //0x00394558
            *enemy = { x, y };
            return;

        default:
            continue;
        }


    }
    *enemy = { 0, 0 };
}


void searchThread(const int width, const int height, DWORD* pPixels, Point* enemy) {

    while (true) {
        radialSearch(width, height, pPixels, enemy);
    }
}


    const HDC displayHandle = GetDC(NULL);
    const HDC BitmapDC = CreateCompatibleDC(displayHandle);

    const int displayWidth = GetDeviceCaps(displayHandle, HORZRES);
    const int displayHeight = GetDeviceCaps(displayHandle, VERTRES);

    BITMAPINFO bmi = {
        bmi.bmiHeader.biSize = sizeof(bmi.bmiHeader),
        bmi.bmiHeader.biWidth = displayWidth,
        bmi.bmiHeader.biHeight = -displayHeight,
        bmi.bmiHeader.biPlanes = 1,
        bmi.bmiHeader.biBitCount = 32
    };

    Point enemy = { 0, 0 };


    DWORD* pPixels = static_cast<DWORD*>(GlobalAlloc(GMEM_FIXED, displayWidth * displayHeight * 4));

    HBITMAP hBitmap = CreateCompatibleBitmap(displayHandle, displayWidth, displayHeight);


int main() {

    SelectObject(BitmapDC, hBitmap);        //  BitmapDC now contains hBitmap

    welcomeMessage();

    std::thread  threadClicker(click, &enemy);

    std::thread  threadAimer(searchThread, displayWidth, displayHeight, pPixels, &enemy);

    while (true) {
        BitBlt(BitmapDC, 0, 0, displayWidth, displayHeight, displayHandle, 0, 0, SRCCOPY);

        GetDIBits(BitmapDC, hBitmap, 0, displayHeight, pPixels, &bmi, DIB_RGB_COLORS);
        Sleep(1);
    }

}





