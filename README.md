# AutoAim

*This is An Aimbot To use in Bopz.io. Made by Midi & Kilian*

### Current Functionality

Aims at the default skincolors of the player models located on the main monitor. However, this includes their hands which arent part of the hitbox. This is achieved by scanning every 4th pixel on the screen using radial search going from last found location or center of the screen.
